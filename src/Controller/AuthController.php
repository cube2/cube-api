<?php
namespace App\Controller;

use App\Document\Roles;
use App\Document\User;
use App\Services\MailerService;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;

class AuthController extends AbstractController
{
    private $dm;
    private $pass;

    public function __construct(DocumentManager $dm,UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->dm = $dm;
        $this->pass = $passwordEncoder;

    }

    /**
     * @Route ("/register", name="register",methods={"POST"})
     */
    public function registerUser(Request $request, MailerService $mailer)
    {
        $data  = json_decode((string)$request->getContent(),true);

        $User =  $this->dm->getRepository(User::class)->findOneBy(['email' => $data['email']]);
        if ($User !== null){
            return new JsonResponse([
                'status' => 403 ,
                'message' => 'Email exist'
            ]);
        }else{
            $rolesCont =  $this->dm->getRepository(Roles::class)->findOneBy(['name' => 'Contributeur']);
            $roleContri = $rolesCont->getName();

            $validator = Validation::createValidator();
            $violations = $validator->validate($data['firstname'], [
                new NotBlank(),
            ]);
            
            if (0 !== count($violations)) {
                return new JsonResponse([
                    'status' => 403 ,
                    'message' => 'field firstname empty'
                ]);
            }
            $violations = $validator->validate($data['lastname'], [
                new NotBlank(),
            ]);
            if (0 !== count($violations)) {
                return new JsonResponse([
                    'status' => 403 ,
                    'message' => 'field lastname empty'
                ]);
            }
            $violations = $validator->validate($data['password'], [
                new NotBlank(),
            ]);
            if (0 !== count($violations)) {
                return new JsonResponse([
                    'status' => 403 ,
                    'message' => 'field password empty'
                ]);
            }
            $violations = $validator->validate($data['email'], [
                new Email(),
                new NotBlank(),
            ]);
            if (0 !== count($violations)) {
                return new JsonResponse([
                    'status' => 403 ,
                    'message' => 'field email vide or not valid'
                ]);
            }
            $user = new User();
            $token = bin2hex(random_bytes(20));
            $user->setFirstname($data['firstname'])
                ->setLastname($data['lastname'])
                ->setPhone($data['phone'])
                ->setIsActive(0)
                ->setCreatedAt(new \DateTime())
                ->setTokenExpiredAt(new \DateTime())
                ->setEmail($data['email'])
                ->setPathPicture($data['picture'])
                ->setToken($token)
                ->setPassword($this->pass->encodePassword(
                    $user,$data['password']
                ))
                ->setRole($roleContri);
            $this->dm->persist($user);
            $this->dm->flush();

            // envoyer un email pour activer le compte
            $subject = 'Agora';
            $template = 'email/confirmEmail.html.twig';
            $params = [
                'id' =>'?id='.$user->getId(),
                'url' => $_ENV["URL_FRONT"].'emailConfirmed/'
            ];
            $mailer->sendEmail($data['email'],$subject,$template,$params);
            return $this->json([
                'status' => 202,
                'message' => 'User '.$data['firstname'].' is register',
            ]);
        }


    }

    /**
     * @Route ("/emailConfirmed/{id}", name="emailConfirmed",methods={"GET"})
     */
    public function emailConfirmed($id): Response
    {
        $User =  $this->dm->getRepository(User::class)->find($id);
        if (!empty($User)){
            $User->setVerifiedEmail(true);
            $User->setIsActive(true);
            $this->dm->persist($User);
            $this->dm->flush();
            return $this->json([
               'status'=> 202,
               'message'=> 'confirmed email',
                'user' => [
                    'id' => $User->getId(),
                    'token' => $User->getToken(),
                    'firstname' => $User->getFirstname(),
                    'lastname' => $User->getLastname(),
                    'picture' => $User->getPathPicture(),
                    'role' => $User->getRole(),
                ]
            ]);
        }
        return $this->json([
            'status'=> 404,
            'message'=> 'Error'
        ]);
    }

    /**
     * @Route ("/login", name="login",methods={"POST"})
     */
    public function loginUser(Request $request): Response
    {
        $data = json_decode((string) $request->getContent(),true);
        $validator = Validation::createValidator();

        $violations = $validator->validate($data['email'], [
            new Email(),
            new NotBlank(),
        ]);
        if (0 !== count($violations)) {
                return new JsonResponse([
                    'status' => 403 ,
                    'message' => 'Email invalide'
                    ]);
        }else{
            $User =  $this->dm->getRepository(User::class)->findOneBy(['email' => $data['email']]);
            if ($User == null){
                return $this->json([
                    'status' => 404,
                    'message' => 'User not found'
                ]);
            }else{
                if ($this->pass->isPasswordValid($User, $data['password'])){
                    return $this->json([
                        'status' => 202,
                        'message' => 'Success password',
                        'user' => [
                            'id' => $User->getId(),
                            'token' => $User->getToken(),
                            'firstname' => $User->getFirstname(),
                            'lastname' => $User->getLastname(),
                            'picture' => $User->getPathPicture(),
                            'role' => $User->getRole(),
                        ]
                    ]);
                }else{
                    return $this->json([
                        'status' => 403,
                        'message' => 'Password invalid'
                    ]);
                }
            }
        }


    }

    /**
     * @Route ("/check_email", name="check_email",methods={"POST"})
     */
    public function checkEmail(Request $request): Response
    {
        $data = json_decode((string) $request->getContent(),true);

        $validator = Validation::createValidator();
        $violations = $validator->validate($data['email'], [
            new Email(),
            new NotBlank(),
        ]);
        if (0 !== count($violations)) {
            return new JsonResponse([
                'status' => 403 ,
                'message' => 'Email invalid'
            ]);
        }else{
            $User =  $this->dm->getRepository(User::class)->findOneBy(['email' => $data['email']]);
            if (empty($User)){
                return new JsonResponse([
                    'status' => 202 ,
                    'message' => 'Email not found'
                ]);
            }else{
                return new JsonResponse([
                    'status' => 404 ,
                    'message' => 'Email exist'
                ]);
            }
        }
    }
    

}
