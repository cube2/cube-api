<?php


namespace App\Controller;


use App\Document\Contents;
use App\Document\Events;
use App\Document\Groups;
use App\Document\User;
use App\Services\CheckTokenService;
use Doctrine\ODM\MongoDB\DocumentManager;
use Laminas\EventManager\Event;
use Pusher\Pusher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContentsController extends AbstractController
{
    private $dm;

    public function __construct(DocumentManager $dm)
    {
        $this->dm = $dm;
    }

    /**
     * @Route("/send_content", name="send_content",methods={"POST"})
     */
    public function createContent(Request $req,Pusher $pusher): Response
    {
        $userId = $req->headers->get('id');
        $userToken =   $req->headers->get('token');
        $data  = json_decode((string)$req->getContent(),true);
        $userValid= new CheckTokenService($this->dm);
        $userValid= $userValid->validUser($userId,$userToken);
        if ($userValid){
            $group =  $this->dm->getRepository(Groups::class)->find($data["id_group"]);
            $user =  $this->dm->getRepository(User::class)->find($userId);
            if ($group){
                $content = new Contents();
                $content->setAuthor($user->getId())
                    ->setAuthorPicture($user->getPathPicture())
                    ->setType($data["type"])
                    ->setValue($data["value"])
                    ->setDate();
                $group->addContents($content);
                $this->dm->persist($group);
                $this->dm->flush();

                $Content = [
                    'id'=> $content->getId(),
                    "id_group" => $content->getGroupId()->getId(),
                    'value'=> $content->getValue(),
                    'author'=> [
                        "id" => $user->getId(),
                        "firstname" => $user->getFirstname(),
                        "lastname" => $user->getLastname(),
                        "picture" => $user->getPathPicture(),
                    ],
                    'createdAt'=> $content->getDate(),
                    'type'=> $content->getType(),
                ];
                $pusher->trigger('group_'.$data["id_group"], 'receiveMessage',$Content);

                return new JsonResponse([
                    'status' => 202 ,
                    'message' => 'Content create'
                ]);
            }else{

                return new JsonResponse([
                    'status' => 404 ,
                    'message' => 'Group not found'
                ]);
            }
        }
        return new JsonResponse([
            'status' => 404 ,
            'message' => 'User not found'
        ]);
    }

    /**
     * @Route("/send_content_event", name="send_content_event",methods={"POST"})
     */
    public function createContentEvent(Request $req,Pusher $pusher): Response
    {
        $userId = $req->headers->get('id');
        $userToken =   $req->headers->get('token');
        $data  = json_decode((string)$req->getContent(),true);
        $userValid= new CheckTokenService($this->dm);
        $userValid= $userValid->validUser($userId,$userToken);
        if ($userValid){
            $event =  $this->dm->getRepository(Events::class)->find($data["id_event"]);
            $user =  $this->dm->getRepository(User::class)->find($userId);
            if ($event){
                $content = new Contents();
                $content->setAuthor($user->getId())
                    ->setAuthorPicture($user->getPathPicture())
                    ->setType($data["type"])
                    ->setValue($data["value"])
                    ->setDate();
                $event->addContent($content);
                $this->dm->persist($event);
                $this->dm->flush();

                $Content = [
                    'id'=> $content->getId(),
                    "id_event" => $content->getEventId()->getId(),
                    'value'=> $content->getValue(),
                    'author'=> [
                        "id" => $user->getId(),
                        "firstname" => $user->getFirstname(),
                        "lastname" => $user->getLastname(),
                        "picture" => $user->getPathPicture(),
                    ],
                    'createdAt'=> $content->getDate(),
                    'type'=> $content->getType(),
                ];
                $pusher->trigger('event_'.$data["id_event"], 'receiveMessage',$Content);

                return new JsonResponse([
                    'content_eve'=>$content,
                    'status' => 202 ,
                    'message' => 'Content create in event'
                ]);
            }else{

                return new JsonResponse([
                    'status' => 404 ,
                    'message' => 'Event not found'
                ]);
            }
        }
        return new JsonResponse([
            'status' => 404 ,
            'message' => 'User not found'
        ]);
    }
}