<?php


namespace App\Controller;


use App\Document\Contents;
use App\Document\Events;
use App\Document\Groups;
use App\Document\Members;
use App\Document\Participants;
use App\Document\User;
use App\Services\CheckTokenService;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EventsController extends AbstractController
{
    private $dm;

    public function __construct(DocumentManager $dm)
    {
        $this->dm = $dm;
    }

    /**
     * @Route("/create_event", name="create_event",methods={"POST"})
     */
    public function createEvent(Request $req): Response
    {
        $userId = $req->headers->get('id');
        $userToken = $req->headers->get('token');
        $data = json_decode((string)$req->getContent(), true);
        $userValid = new CheckTokenService($this->dm);
        $userValid = $userValid->validUser($userId, $userToken);
        if ($userValid) {
            $getGroup = $this->dm->getRepository(Groups::class)->find($data['id_group']);
            $getMembres = $this->dm->getRepository(Members::class)->findBy(['groupId' => $data['id_group']]);
            $membres = [];
            foreach ($getMembres as $membre) {
                $particiants = new Participants();
                $particiants->setIdUser($membre->getUserId());
                $particiants->setIsPresent(true);
                array_push($membres, $particiants);
            }

            $events = new Events();
            $events->setCreatedAt(new \DateTime());
            $events->setDate($data['date_event']);
            $events->setAuthorId($userId);
            $events->setDescription($data['description']);
            $events->setName($data['name']);
            $events->setLocation($data['location']);
            $events->setPicture($data['picture']);
            $events->setGroupId($getGroup);
            $events->addMenbres($membres);
            $this->dm->persist($events);
            $this->dm->flush();

            return new JsonResponse([
                'status' => 202,
                'message' => 'Create event',
                'id_event' => $events->getId()
            ]);
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'User not found'
        ]);
    }

    /**
     * @Route("/get_event/{idEvent}", name="get_event_contents",methods={"GET"})
     */
    public function getEvent(Request $req, $idEvent): Response
    {
        $userId = $req->headers->get('id');
        $userToken = $req->headers->get('token');
        $userValid = new CheckTokenService($this->dm);
        $userValid = $userValid->validUser($userId, $userToken);
        if ($userValid) {
            $event = $this->dm->getRepository(Events::class)->find($idEvent);
            $contents = $this->dm->getRepository(Contents::class)->getAllContentForIdEvent($idEvent);
            $participants = $this->dm->getRepository(Participants::class)->findBy(['eventsId' => $idEvent]);
            $authorEvent = $this->dm->getRepository(User::class)->findOneBy(['id' => $event->getAuthorId()]);
            if ($event) {
                $Events = [
                    'id' => $event->getId(),
                    'id_group' => $event->getGroupId()->getId(),
                    'name' => $event->getName(),
                    'description' => $event->getDescription(),
                    'date_event' => $event->getDate(),
                    'location' => $event->getLocation(),
                    'createdAt' => $event->getCreatedAt(),
                    'picture' => $event->getPicture(),
                    'author' => [
                        "id" => $authorEvent->getId(),
                        "firstname" => $authorEvent->getFirstname(),
                        "lastname" => $authorEvent->getLastname(),
                        "picture" => $authorEvent->getPathPicture(),
                    ],
                    "contents" => [],
                    "participants" => [],
                ];
                foreach ($participants as $participant) {
                    $UserMember = $this->dm->getRepository(User::class)->find($participant->getIdUser());
                    $data = [
                        'isPresent' => $participant->getIsPresent(),
                        'author' => [
                            "id" => $UserMember->getId(),
                            "firstname" => $UserMember->getFirstname(),
                            "lastname" => $UserMember->getLastname(),
                            "picture" => $UserMember->getPathPicture(),
                        ],
                    ];
                    array_push($Events["participants"], $data);
                }
                foreach ($contents as $content) {
                    $UserContent = $this->dm->getRepository(User::class)->find($content->getAuthor());
                    $data = [
                        'id' => $content->getId(),
                        "id_event" => $content->getEventId()->getId(),
                        'value' => $content->getValue(),
                        'author' => [
                            "id" => $UserContent->getId(),
                            "firstname" => $UserContent->getFirstname(),
                            "lastname" => $UserContent->getLastname(),
                            "picture" => $UserContent->getPathPicture(),
                        ],
                        'createdAt' => $content->getDate(),
                        'type' => $content->getType(),
                    ];
                    array_push($Events["contents"], $data);
                }
                return $this->json($Events);
            } else {

                return new JsonResponse([
                    'status' => 404,
                    'message' => 'Event not found'
                ]);
            }
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'User not found'
        ]);
    }
    /**
     * @Route("/update_event/{idEvent}", name="update_event",methods={"PUT"})
     */
    public function UpdateGroup(Request $req,$idEvent): Response
    {
        $userId = $req->headers->get('id');
        $userToken = $req->headers->get('token');
        $data = json_decode((string)$req->getContent(), true);
        $userValid = new CheckTokenService($this->dm);
        $userValid = $userValid->validUser($userId, $userToken);
        if ($userValid) {
            $event =  $this->dm->getRepository(Events::class)->find($idEvent);
            if ($event->getAuthorId() === $userId)
            {
                $event->setName($data["name"]);
                $event->setDescription($data["description"]);
                $event->setDate($data["dateEvent"]);
                $event->setLocation($data["location"]);
                $event->setPicture($data["picture"]);
                $this->dm->flush();
                return $this->json([
                    'status' => 202,
                    'message' => 'Event update'
                ]);
            }else{
                return $this->json([
                    'status' => 404,
                    'message' => 'He is not author of event'
                ]);
            }
        } else {
            return $this->json([
                'status' => 404,
                'message' => 'information user is not valid'
            ]);
        }
    }

    /**
     * @Route("/user_future_events", name="user_future_events",methods={"GET"})
     */
    public function getUserGroup(Request $req): Response
    {
        $userId = $req->headers->get('id');
        $userToken = $req->headers->get('token');
        $userValid = new CheckTokenService($this->dm);
        $userValid = $userValid->validUser($userId, $userToken);
        if ($userValid) {
            $dateEvent = new \DateTime();
            $events = $this->dm->getRepository(Events::class)->findBy(["authorId" => $userId]);
            if (!$events) {
                return new JsonResponse([
                    'status' => 404,
                    'message' => 'No event found'
                ]);
            } else {
                $eventsUser = [];
                foreach ($events as $eventUser) {
                    if ($eventUser->getDate() > $dateEvent)
                    {
                        $authorEvent = $this->dm->getRepository(User::class)->find($eventUser->getAuthorId());
                        $group = $this->dm->getRepository(Groups::class)->find($eventUser->getGroupId()->getId());
                        $event = [
                            'id'=> $eventUser->getId(),
                            'name'=> $eventUser->getName(),
                            'description'=> $eventUser->getDescription(),
                            'date_event'=> $eventUser->getDate()->format('Y-m-d H:i:s'),
                            'location'=> $eventUser->getLocation(),
                            'createdAt'=> $eventUser->getCreatedAt(),
                            'picture'=> $eventUser->getPicture(),
                            'group_picture'=> $group->getPicture(),
                            'author'=> [
                                "id" => $authorEvent->getId(),
                                "firstname" => $authorEvent->getFirstname(),
                                "lastname" => $authorEvent->getLastname(),
                                "picture" => $authorEvent->getPathPicture(),
                            ],
                        ];
                        array_push($eventsUser, $event);
                    }
                }
                return new JsonResponse($eventsUser);
            }
        } else {
            return $this->json([
                'status' => 404,
                'message' => 'information user is not valid'
            ]);
        }
    }

}