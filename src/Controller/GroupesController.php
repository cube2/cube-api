<?php


namespace App\Controller;


use App\Document\Contents;
use App\Document\Events;
use App\Document\Groups;
use App\Document\Members;
use App\Document\Participants;
use App\Document\Roles;
use App\Document\User;
use App\Services\CheckTokenService;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;

class GroupesController extends AbstractController
{
    private $dm;
    private $pass;

    public function __construct(DocumentManager $dm, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->dm = $dm;

        $this->pass = $passwordEncoder;

    }

    /**
     * @Route("/created_group", name="created_group",methods={"POST"})
     */
    public function createGroup(Request $req): Response
    {
        $userId = $req->headers->get('id');
        $userToken = $req->headers->get('token');
        $data = json_decode((string)$req->getContent(), true);
        $defaultVal = false;
        $userValid = new CheckTokenService($this->dm);
        $userValid = $userValid->validUser($userId, $userToken);
        if ($userValid) {
            if (empty($userId) || empty($data['name']) || empty($data['description']) || empty($data['picture'])) {
                return new JsonResponse([
                    'status' => 404,
                    'message' => 'the data are not filled in'
                ]);
            }
            $user = $this->dm->getRepository(User::class)->find($userId);
            $role = $this->dm->getRepository(Roles::class)->findOneBy(['name' => 'Administrateur']);

            $menbre = new Members();
            $menbre->setRole($role->getRole())
                ->setUserId($user->getId())
                ->setIsBanned($defaultVal)
                ->setLastRead()
                ->setPendingInvitation($defaultVal);

            $group = new Groups();
            $group->setCreatedAt(new \DateTime())
                ->setName($data['name'])
                ->setPicture($data['picture'])
                ->setTheme($data['theme'])
                ->setIsPublic($data['isPublic'])
                ->setDescription($data['description'])
                ->addMenbres([$menbre]);
            $this->dm->persist($group);
            $this->dm->flush();

            return new JsonResponse([
                'status' => 202,
                'message' => 'Group created',
                'id_group' => $group->getId(),
            ]);
        }
        return new JsonResponse([
            'status' => 404,
            'message' => 'User not found'
        ]);
    }

    /**
     * @Route("/all_public_groups", name="all_public_groups",methods={"GET"})
     */
    public function ALLGroupPublic(Request $req): Response
    {
        $userId = $req->headers->get('id');
        $userToken = $req->headers->get('token');
        $userValid = new CheckTokenService($this->dm);
        $userValid = $userValid->validUser($userId, $userToken);
        if ($userValid) {
            $groupPublic = $this->dm->getRepository(Groups::class)->findBy(["isPublic" => true]);
            $groupsPublic = [];

            foreach ($groupPublic as $groups) {
                $group = [
                    "id" => $groups->getId(),
                    "name" => $groups->getName(),
                    "description" => $groups->getDescription(),
                    "picture" => $groups->getPicture(),
                ];
                array_push($groupsPublic, $group);
            }
            return $this->json($groupsPublic);
        }
        return $this->json([
            'status' => 404,
            'message' => 'information user is not valid'
        ]);
    }

    /**
     * @Route("/all_groups", name="all_groups",methods={"GET"})
     */
    public function ALLGroup(Request $req): Response
    {
        $userId = $req->headers->get('id');
        $userToken = $req->headers->get('token');
        $userValid = new CheckTokenService($this->dm);
        $userValid = $userValid->validUser($userId, $userToken);
        if ($userValid) {
            $groupPublic = $this->dm->getRepository(Groups::class)->findAll();
            $groupsPublic = [];

            foreach ($groupPublic as $groups) {
                $group = [
                    "id" => $groups->getId(),
                    "name" => $groups->getName(),
                    "description" => $groups->getDescription(),
                    "picture" => $groups->getPicture(),
                ];
                array_push($groupsPublic, $group);
            }
            return $this->json($groupsPublic);
        }
        return $this->json([
            'status' => 404,
            'message' => 'information user is not valid'
        ]);
    }

    /**
     * @Route("/delete_group", name="delete_group",methods={"POST"})
     */
    public function deleteGroup(Request $req): Response
    {
        $userId = $req->headers->get('id');
        $userToken = $req->headers->get('token');
        $data = json_decode((string)$req->getContent(), true);
        $userValid = new CheckTokenService($this->dm);
        $userValid = $userValid->validUser($userId, $userToken);
        if ($userValid) {
            $validator = Validation::createValidator();
            $violations = $validator->validate($userId, [
                new NotBlank(),
            ]);
            if (0 !== count($violations)) {
                return new JsonResponse([
                    'status' => 403,
                    'message' => 'field id empty'
                ]);
            }

            $Groupe = $this->dm->getRepository(Groups::class)->find($data['group_id']);
            if (!$Groupe) {
                return new JsonResponse([
                    'status' => 404,
                    'message' => 'No group found'
                ]);
            } else {
                $this->dm->remove($Groupe);
                $this->dm->flush();
                return new JsonResponse([
                    'status' => 202,
                    'message' => 'Group delete'
                ]);
            }
        } else {
            return $this->json([
                'status' => 404,
                'message' => 'information user is not valid'
            ]);
        }
    }

    /**
     * @Route("/user_groups", name="user_groups",methods={"GET"})
     */
    public function getUserGroup(Request $req): Response
    {
        $userId = $req->headers->get('id');
        $userToken = $req->headers->get('token');
        $userValid = new CheckTokenService($this->dm);
        $userValid = $userValid->validUser($userId, $userToken);
        if ($userValid) {
            $Menbre = $this->dm->getRepository(Members::class)->findBy(["userId" => $userId]);
            if (!$Menbre) {
                return new JsonResponse([
                    'status' => 404,
                    'message' => 'No  group found'
                ]);
            } else {
                $groupUser = [];
                foreach ($Menbre as $idgroup) {
                    $idgroup = $idgroup->getGroupId()->getId();
                    $group = $this->dm->getRepository(Groups::class)->find($idgroup);
                    $lastContent = $this->dm->getRepository(Contents::class)->findBy(['groupId' => $idgroup], ['createdAt' => 'DESC']);
                    if (!empty($lastContent)) {
                        $lastContent = $lastContent[0];
                        $UserContent = $this->dm->getRepository(User::class)->find($lastContent->getAuthor());
                        $groups = [
                            "id" => $group->getId(),
                            "name" => $group->getName(),
                            "description" => $group->getDescription(),
                            "picture" => $group->getPicture(),
                            "isPublic" => $group->getIsPublic(),
                            "theme" => $group->getTheme(),
                            "createdAt" => $group->getCreatedAt(),
                            "unreadContent" => 1,
                            "members" => [],
                            "contents" => [
                                [
                                    "id" => $lastContent->getId(),
                                    "id_group" => $group->getId(),
                                    "value" => $lastContent->getValue(),
                                    "type" => $lastContent->getType(),
                                    'author' => [
                                        "id" => $UserContent->getId(),
                                        "firstname" => $UserContent->getFirstname(),
                                        "lastname" => $UserContent->getLastname(),
                                        "picture" => $UserContent->getPathPicture(),
                                    ],
                                    "authorPicture" => $lastContent->getAuthorPicture(),
                                    "createdAt" => $lastContent->getDate(),
                                ],
                            ]
                        ];
                    } else {
                        $groups = [
                            "id" => $group->getId(),
                            "name" => $group->getName(),
                            "description" => $group->getDescription(),
                            "picture" => $group->getPicture(),
                            "isPublic" => $group->getIsPublic(),
                            "theme" => $group->getTheme(),
                            "createdAt" => $group->getCreatedAt(),
                            "members" => [],
                            "contents" => []
                        ];
                    }
                    array_push($groupUser, $groups);
                }
                return new JsonResponse($groupUser);
            }
        } else {
            return $this->json([
                'status' => 404,
                'message' => 'information user is not valid'
            ]);
        }
    }

    /**
     * @Route("/get_group/{idGroup}", name="get_group_contents",methods={"GET"})
     */
    public function AllGroupContent(Request $req,$idGroup): Response
    {
        $userId = $req->headers->get('id');
        $userToken =   $req->headers->get('token');
        $userValid= new CheckTokenService($this->dm);
        $userValid= $userValid->validUser($userId,$userToken);
        if ($userValid){
            $group =  $this->dm->getRepository(Groups::class)->find($idGroup);
            $contents = $this->dm->getRepository(Contents::class)->getAllContentForIdGroup($idGroup);
            $members = $this->dm->getRepository(Members::class)->findBy(['groupId'=>$idGroup,'isBanned'=> false]);
            $events = $this->dm->getRepository(Events::class)->findBy(['groupId' => $idGroup],['dateEvent'=>'ASC']);
            if ($group){
                $contentGroup= [
                        "id"=> $group->getId(),
                        "name"=> $group->getName(),
                        "description"=> $group->getDescription(),
                        "createdAt"=> $group->getCreatedAt(),
                        "picture"=> $group->getPicture(),
                        "theme"=> $group->getTheme(),
                        "isPublic"=> $group->getIsPublic(),
                        "contents"=>[],
                        "members"=>[],
                        "events"=>[],
                ];
                foreach ($events as $event) {
                    $authorEvent = $this->dm->getRepository(User::class)->find($event->getAuthorId());
                    $data =[
                        'id'=> $event->getId(),
                        'name'=> $event->getName(),
                        'description'=> $event->getDescription(),
                        'date_event'=> $event->getDate(),
                        'location'=> $event->getLocation(),
                        'createdAt'=> $event->getCreatedAt(),
                        'picture'=> $event->getPicture(),
                        'author'=> [
                            "id" => $authorEvent->getId(),
                            "firstname" => $authorEvent->getFirstname(),
                            "lastname" => $authorEvent->getLastname(),
                            "picture" => $authorEvent->getPathPicture(),
                        ],
                    ];
                    array_push($contentGroup["events"],$data);
                }
                foreach ($members as $member) {
                    $UserMember = $this->dm->getRepository(User::class)->find($member->getUserId());
                    $data =[
                        'role'=> $member->getRole(),
                        'isBanned'=> $member->getIsBanned(),
                        'pendingInvitation'=> $member->getPendingInvitation(),
                        'lastRead'=> $member->getLastRead(),
                        'author'=> [
                            "id" => $UserMember->getId(),
                            "firstname" => $UserMember->getFirstname(),
                            "lastname" => $UserMember->getLastname(),
                            "picture" => $UserMember->getPathPicture(),
                        ],
                    ];
                    array_push($contentGroup["members"],$data);
                }
                foreach ($contents as $content){
                    $UserContent = $this->dm->getRepository(User::class)->find($content->getAuthor());
                    $data =[
                        'id'=> $content->getId(),
                        "id_group" => $content->getGroupId()->getId(),
                        'value'=> $content->getValue(),
                        'author'=> [
                            "id" => $UserContent->getId(),
                            "firstname" => $UserContent->getFirstname(),
                            "lastname" => $UserContent->getLastname(),
                            "picture" => $UserContent->getPathPicture(),
                        ],
                        'createdAt'=> $content->getDate(),
                        'type'=> $content->getType(),
                    ];
                    array_push($contentGroup["contents"],$data);
                }
                return $this->json($contentGroup);
            }else{

                return new JsonResponse([
                    'status' => 404 ,
                    'message' => 'Group not found'
                ]);
            }
        }
        return new JsonResponse([
            'status' => 404 ,
            'message' => 'User not found'
        ]);
    }

    /**
     * @Route("/update_group/{idGroup}", name="update_group",methods={"PUT"})
     */
    public function UpdateGroup(Request $req,$idGroup): Response
    {
        $userId = $req->headers->get('id');
        $userToken = $req->headers->get('token');
        $data = json_decode((string)$req->getContent(), true);
        $userValid = new CheckTokenService($this->dm);
        $userValid = $userValid->validUser($userId, $userToken);
        if ($userValid) {
            $role = "Admin";
            $group =  $this->dm->getRepository(Groups::class)->find($idGroup);
            $adminGroup =  $this->dm->getRepository(Members::class)->findOneBy(['groupId'=>$idGroup,'role'=>$role]);
            if ($adminGroup->getUserId() === $userId)
            {
                $group->setName($data["name"]);
                $group->setDescription($data["description"]);
                $group->setPicture($data["picture"]);
                $group->setTheme($data["theme"]);
                $this->dm->flush();
                return $this->json([
                    'status' => 202,
                    'message' => 'Group update'
                ]);
            }else{
                return $this->json([
                    'status' => 404,
                    'message' => 'He is not admin of group'
                ]);
            }
        } else {
            return $this->json([
                'status' => 404,
                'message' => 'information user is not valid'
            ]);
        }
    }
    /**
     * @Route("/update_membres/{idGroup}", name="update_membres",methods={"PUT"})
     */
    public function UpdateMembres(Request $req,$idGroup): Response
    {
        $userId = $req->headers->get('id');
        $userToken = $req->headers->get('token');
        $data = json_decode((string)$req->getContent(), true);
        $userValid = new CheckTokenService($this->dm);
        $userValid = $userValid->validUser($userId, $userToken);
        if ($userValid) {
    //// logique update membres
        } else {
            return $this->json([
                'status' => 404,
                'message' => 'information user is not valid'
            ]);
        }
    }

    /**
     * @Route("/leave_group/{idGroup}", name="update_membres",methods={"GET"})
     */
    public function LeaveGroup(Request $req,$idGroup): Response
    {
        $userId = $req->headers->get('id');
        $userToken = $req->headers->get('token');
        $data = json_decode((string)$req->getContent(), true);
        $userValid = new CheckTokenService($this->dm);
        $userValid = $userValid->validUser($userId, $userToken);
        if ($userValid) {
            $members = $this->dm->getRepository(Members::class)->findOneBy(['groupId'=>$idGroup,'userId'=> $userId]);
            $members->setIsBanned(true);

            $this->dm->persist($members);
            $this->dm->flush();

            return $this->json([
                'status' => 202,
                'message' => 'Member is banned'
            ]);

        } else {
            return $this->json([
                'status' => 404,
                'message' => 'information user is not valid'
            ]);
        }
    }
    /**
     * @Route("/delete_group/{id}", name="delete_group",methods={"DELETE"})
     */
    public function DeletGroup(Request $req,$id): Response
    {
        $userId = $req->headers->get('id');
        $userToken = $req->headers->get('token');
        $userValid = new CheckTokenService($this->dm);
        $userValid = $userValid->validUser($userId, $userToken);
        if ($userValid) {
            $group = $this->dm->getRepository(Groups::class)->find($id);
            $contents = $this->dm->getRepository(Contents::class)->findBy(['groupId'=>$group->getId()]);
            $members = $this->dm->getRepository(Members::class)->findBy(['groupId'=>$group->getId()]);
            foreach ($contents as $content){
                $this->dm->remove($content);
                $this->dm->flush();
            }
            foreach ($members as $member){
                $this->dm->remove($member);
                $this->dm->flush();
            }
            $events = $this->dm->getRepository(Events::class)->findBy(['groupId'=>$group->getId()]);
            foreach ($events as $event){
                $contents = $this->dm->getRepository(Contents::class)->findBy(['eventId'=>$event->getId()]);
                $participants = $this->dm->getRepository(Participants::class)->findBy(['eventId'=>$event->getId()]);
                foreach ($participants as $participant){
                    $this->dm->remove($participant);
                    $this->dm->flush();
                }
                foreach ($contents as $content){
                    $this->dm->remove($content);
                    $this->dm->flush();
                }
                $this->dm->remove($event);
                $this->dm->flush();
            }
            $this->dm->remove($group);
            $this->dm->flush();
            return $this->json([
                'status' => 202,
                'message' => 'group delete'
            ]);
        }
        return $this->json([
            'status' => 404,
            'message' => 'information user is not valid'
        ]);
    }

}