<?php


namespace App\Controller;


use App\Document\Groups;
use App\Document\Members;
use App\Document\Roles;
use App\Document\User;
use App\Services\CheckTokenService;
use App\Services\MailerService;
use Doctrine\ODM\MongoDB\DocumentManager;
use http\Exception\BadConversionException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use function Symfony\Component\String\u;

class MenbresController extends AbstractController
{
    private $dm;
    private $pass;

    public function __construct(DocumentManager $dm, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->dm = $dm;

        $this->pass = $passwordEncoder;

    }
    /**
     * @Route("/add_members", name="add_members",methods={"POST"})
     */
    public function createMember(Request $req,MailerService $mailer): Response
    {
        $userId = $req->headers->get('id');
        $userToken = $req->headers->get('token');
        $data  = json_decode((string)$req->getContent(),true);
        $defaultVal = false;
        $userValid= new CheckTokenService($this->dm);
        $userValid= $userValid->validUser($userId,$userToken);
        $userAdminInfo =  $this->dm->getRepository(User::class)->find($userId);
        if ($userValid){
            $group =  $this->dm->getRepository(Groups::class)->find($data['id']);
            if ($group){
                $membres = [];
                foreach ($data['user_ids'] as $user_id){
                        $user =  $this->dm->getRepository(User::class)->find($user_id);
                        if ($user){
                            $checkUser =  $this->dm->getRepository(Members::class)->findOneBy(['group' => $group->getId(),'userId'=>$user->getId()]);
                            if ($checkUser){
                                return new JsonResponse([
                                    'status' => 404 ,
                                    'message' => 'Member exist'
                                ]);
                            }
                            if ($group->getIsPublic()){
                                $role =  $this->dm->getRepository(Roles::class)->findOneBy(['name' => 'Contributeur']);
                            }else{
                                $role =  $this->dm->getRepository(Roles::class)->findOneBy(['name' => 'Invité']);
                            }
                            $membre = new Members();
                            $membre->setRole($role->getRole())
                                ->setUserId($user->getId())
                                ->setIsBanned($defaultVal)
                                ->setLastRead()
                                ->setPendingInvitation($defaultVal);

                            array_push($membres,$membre);

                            // send email for confirm invitation group
                            $subject = 'Confirmation d\'invitation au groupe '.$group->getName();
                            $template = 'email/validMenbre.html.twig';
                            $params = [
                                'userAdmin' => $userAdminInfo->getFirstname(),
                                'groupName' => $group->getName(),
                                'user' => $user->getFirstname().' '.$user->getLastname(),
                                'url' => $_ENV["URL_FRONT"].'confirmInvitation/?id='.$group->getId()
                            ];
                            $mailer->sendEmail($user->getEmail(),$subject,$template,$params);
                        }
                        //else{
//                            // send email user not exit BDD
//                            $subject = 'Invitation à rejoindre un groupe sur agora';
//                            $template = 'email/InvitationNewUser.html.twig';
//                            $params = [
//                                'groupName' => $group->getName(),
//                                'user' => $userAdminInfo->getFirstname().' '.$userAdminInfo->getLastname(),
//                                'url' => $_ENV["URL_FRONT"].'/inscription'
//                            ];
//                            $mailer->sendEmail($data['email'],$subject,$template,$params);
//                            return new JsonResponse([
//                                'status' => 404 ,
//                                'message' => 'User not exist'
//                            ]);
//                        }
                    }
                $group->addMenbres($membres);
                $this->dm->persist($group);
                $this->dm->flush();

                return new JsonResponse([
                    'status' => 202 ,
                    'message' => 'add members in group'
                ]);
            }else{
                return new JsonResponse([
                    'status' => 404 ,
                    'message' => 'Group not found'
                ]);
            }
        }
        return new JsonResponse([
            'status' => 404 ,
            'message' => 'User not found'
        ]);
    }

    /**
     * @Route("/confirm_invitation", name="confirm_invitation",methods={"POST"})
     */
    public function confirmInvitation(Request $req): Response
    {
        $userId = $req->headers->get('id');
        $userToken = $req->headers->get('token');
        $data = json_decode((string)$req->getContent(), true);
        $userValid = new CheckTokenService($this->dm);
        $userValid = $userValid->validUser($userId, $userToken);
        if ($userValid) {
            $menbres = $this->dm->getRepository(Members::class)->findOneBy(['userId'=>$userId,'group'=>$data['id_group']]);
            $menbres->setPendingInvitation(true);
            $this->dm->persist($menbres);
            $this->dm->flush();
            return new JsonResponse([
                'status' => 202 ,
                'message' => 'valid invitation'
            ]);
            }else{
            return new JsonResponse([
                'status' => 404 ,
                'message' => 'User not found'
            ]);
        }
    }
}