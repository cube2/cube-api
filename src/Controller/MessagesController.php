<?php


namespace App\Controller;


use App\Document\Contents;
use App\Services\CheckTokenService;
use Doctrine\ODM\MongoDB\DocumentManager;
use Pusher\Pusher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MessagesController extends AbstractController
{
    private $dm;

    public function __construct(DocumentManager $dm)
    {
        $this->dm = $dm;

    }

    /**
     * @Route("/message", name="message",methods={"POST"})
     */
    public function message(Request $req, Pusher $pusher): Response
    {
        $data  = json_decode((string)$req->getContent(),true);
        $data['message'] = 'Salut Theo';
        $data['id'] = ['60293b5d49d18b4e9c4c5932','60293c23eb39df3632404f35'];
        $pusher->trigger('my-channel', 'my-event', $data);

        return $this->json(['ok']);
    }
    /**
     * @Route("/message/group", name="message_group",methods={"POST"})
     */
    public function messageGroup(Request $req): Response
    {
        $userId = $req->headers->get('id');
        $userToken =   $req->headers->get('token');
        $data  = json_decode((string)$req->getContent(),true);
        $userValid= new CheckTokenService($this->dm);
        $userValid= $userValid->validUser($userId,$userToken);
        if ($userValid){
            $contents = [];
            $contentsGroup =  $this->dm->getRepository(Contents::class)->findBy(['groupId'=>$data["id_group"]]);

            foreach ($contentsGroup as $content){
                $data = [
                    "id" => $content->getId(),
                    "id_group" => $content->getGroupId()->getId(),
                    "author" => $content->getAuthor(),
                    "value" => $content->getValue(),
                    "authorPicture" => $content->getAuthorPicture(),
                    "cretedAt" => $content->getDate(),
                    "type" => $content->getType(),
                ];
                array_push($contents,$data);

            }
            return new JsonResponse($contents);
        }
        return new JsonResponse([
            'status' => 404 ,
            'message' => 'User not found'
        ]);
    }

}