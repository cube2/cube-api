<?php


namespace App\Controller;


use App\Document\Groups;
use App\Document\User;
use App\Services\CheckTokenService;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserActionController extends AbstractController
{
    private $dm;

    public function __construct(DocumentManager $dm)
    {
        $this->dm = $dm;
    }

    /**
     * @Route("/get_active_users", name="get_active_user",methods={"GET"})
     */
    public function getActiveUsers(Request $req): Response
    {
        $userId = $req->headers->get('id');
        $userToken =   $req->headers->get('token');
        $userValid= new CheckTokenService($this->dm);
        $userValid= $userValid->validUser($userId,$userToken);
        if ($userValid){
            $finnalyUsers = [];
            $users = $this->dm->getRepository(User::class)->findBy(['isActive' =>true, 'verifiedEmail' => true]);
            foreach ($users as $user){
                $data = [
                    "id" => $user->getId(),
                    "firstname" => $user->getFirstname(),
                    "lastname" => $user->getLastname(),
                    "role" => $user->getRole(),
                    "picture" => $user->getPathPicture(),
                ];
                array_push($finnalyUsers,$data);
            }
            return $this->json($finnalyUsers);
        }else{
            return new JsonResponse([
                'status' => 404 ,
                'message' => 'User not found'
            ]);
        }
    }

    /**
     * @Route("/delete_user/{id}", name="delete_user",methods={"DELETE"})
     */
    public function deleteUser(Request $req,$id): Response
    {
        $userId = $req->headers->get('id');
        $userToken =   $req->headers->get('token');
        $userValid= new CheckTokenService($this->dm);
        $userValid= $userValid->validUser($userId,$userToken);
        if ($userValid){
            $user = $this->dm->getRepository(User::class)->find($id);
            $this->dm->remove($user);
            $this->dm->flush();
            return new JsonResponse([
                'status' => 404 ,
                'message' => 'Delete user'
            ]);
        }else{
            return new JsonResponse([
                'status' => 404 ,
                'message' => 'User not found'
            ]);
        }
    }
}