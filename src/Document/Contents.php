<?php


namespace App\Document;
use App\Repository\ContentsRepository;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass=ContentsRepository::class)
 */
class Contents
{
    /**
     * @MongoDB\Id
     */
    protected $id;


    /**
     * @MongoDB\Field(type="string")
     */
    protected $value;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $author;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $authorPicture;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $createdAt;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $type;


    /**
     * @MongoDB\ReferenceOne(targetDocument=Groups::class, inversedBy="contents", storeAs="id")
     */
    public $groupId;

    /**
     * @MongoDB\ReferenceOne(targetDocument=Groups::class, inversedBy="contents", storeAs="id")
     */
    public $eventId;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Contents
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return Contents
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     * @return Contents
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthorPicture()
    {
        return $this->authorPicture;
    }

    /**
     * @param mixed $authorPicture
     * @return Contents
     */
    public function setAuthorPicture($authorPicture)
    {
        $this->authorPicture = $authorPicture;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->createdAt->format('Y-m-d H:i:s');
    }

    public function setDate()
    {
        $this->createdAt = new \DateTime();
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Contents
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @param mixed $groupId
     */
    public function setGroupId($groupId): void
    {
        $this->groupId = $groupId;
    }

    /**
     * @return mixed
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @param mixed $eventId
     */
    public function setEvents($eventId): void
    {
        $this->eventId = $eventId;
    }




}