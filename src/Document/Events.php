<?php


namespace App\Document;
use App\Repository\EventsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass=EventsRepository::class)
 */

class Events
{

    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $authorId;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $name;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $description;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $dateEvent;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $location;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $createdAt;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $picture;

    /**
     * @MongoDB\ReferenceMany(targetDocument=Participants::class, mappedBy="eventsId", storeAs="id",cascade={"persist"})
     */
    protected $participants;

    /**
     * @MongoDB\ReferenceMany(targetDocument=Contents::class, mappedBy="eventsId", storeAs="id",cascade={"persist"})
     */
    protected $contents;

    /**
     * @MongoDB\ReferenceOne(targetDocument=Groups::class, inversedBy="eventId", storeAs="id")
     */
    public $groupId;


    public function __construct() {
        $this->participants = new ArrayCollection();
        $this->contents = new ArrayCollection();
    }

    public function addMenbres(array $participants): void
    {
        foreach ($participants as $participant){
            $participant->eventsId = $this;
            $this->participants->add($participant);
        }
    }

    public function removeMenbres(Participants $participants): void
    {
        $participants->eventsId = null;
        $this->participants->removeElement($participants);
    }

    public function addContent(Contents $contents): void
    {
            $contents->eventId = $this;
            $this->contents->add($contents);
    }

    public function removeContent(Contents $contents): void
    {
        $contents->eventId = null;
        $this->participants->removeElement($contents);
    }
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->dateEvent;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void
    {
        $this->dateEvent = $date;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     */
    public function setLocation($location): void
    {
        $this->location = $location;
    }

    /**
 * @return mixed
 */
    public function getCreatedAt()
    {
        return $this->createdAt->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @param mixed $groupId
     */
    public function setGroupId(Groups $group): void
    {
        $this->groupId = $group;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture): void
    {
        $this->picture = $picture;
    }

    /**
     * @return mixed
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * @param mixed $authorId
     */
    public function setAuthorId($authorId): void
    {
        $this->authorId = $authorId;
    }



}