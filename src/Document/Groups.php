<?php


namespace App\Document;
use App\Repository\GroupsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\ODM\MongoDB\PersistentCollection;

/**
 * @MongoDB\Document(repositoryClass=GroupsRepository::class)
 */
class Groups
{

    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $name;

    /**
     * @MongoDB\Field(type="string",nullable=true)
     */
    protected $description;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $picture;

    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $isPublic;

    /**
     * @MongoDB\Field(type="string",nullable=true)
     */
    protected $theme;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $createdAt;

    /**
     * @MongoDB\ReferenceMany(targetDocument=Members::class, mappedBy="groupId", storeAs="id",cascade={"persist"})
     */
    protected $menbres;

    /**
     * @MongoDB\ReferenceMany(targetDocument=Contents::class, mappedBy="groupId", storeAs="id",cascade={"persist"}, nullable=true)
     */
    protected $contents;

    /**
     * @MongoDB\ReferenceMany(targetDocument=Events::class, mappedBy="groupId", storeAs="id",cascade={"persist"}, nullable=true)
     */
    protected $eventId;

    public function __construct() {
        $this->contents = new ArrayCollection();
        $this->menbres = new ArrayCollection();
        $this->eventId = new ArrayCollection();
    }

    public function addEvents(Events $events): void
    {

        $events->groupId = $this;
        $this->eventId->add($events);
    }

    public function removeEvents(Events $events): void
    {
        $events->groupId = null;
        $this->menbres->removeElement($events);
    }

    public function addMenbres(array $menbres): void
    {
        foreach ($menbres as $menbre){
            $menbre->groupId = $this;
            $this->menbres->add($menbre);
        }
    }

    public function removeMenbres(Members $menbres): void
    {
        $menbres->groupId = null;
        $this->menbres->removeElement($menbres);
    }

    public function addContents(Contents $contents): void
    {
        $contents->groupId = $this;
        $this->contents->add($contents);
    }

    public function removeContent(Contents $contents): void
    {
        $contents->groupId = null;
        $this->contents->removeElement($contents);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Groups
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Groups
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Groups
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $createdAt
     * @return Groups
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     * @return Groups
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * @param mixed $theme
     * @return Groups
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * @param mixed $isPublic
     * @return Groups
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;
        return $this;
    }



}