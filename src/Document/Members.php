<?php


namespace App\Document;
use App\Repository\MembersRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass=MembersRepository::class)
 */
class Members
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $userId;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $role;

    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $isBanned;

    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $pendingInvitation;

    /**
     * @MongoDB\ReferenceOne(targetDocument=Groups::class, inversedBy="menbres", storeAs="id")
     */
    public $groupId;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $lastRead;

    public function __construct() {
        $this->groupId = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Members
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     * @return Members
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     * @return Members
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsBanned()
    {
        return $this->isBanned;
    }

    /**
     * @param mixed $is_banned
     * @return Members
     */
    public function setIsBanned($is_banned)
    {
        $this->isBanned = $is_banned;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPendingInvitation()
    {
        return $this->pendingInvitation;
    }

    /**
     * @param mixed $pending_invitation
     * @return Members
     */
    public function setPendingInvitation($pending_invitation)
    {
        $this->pendingInvitation = $pending_invitation;
        return $this;
    }
    /**
     * @return mixed
     */
    public function getLastRead()
    {
        return $this->lastRead->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed
     * @return Members
     */
    public function setLastRead()
    {
        $this->lastRead =  new \DateTime();
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGroupId()
    {
        return $this->groupId;
    }


}