<?php


namespace App\Document;
use App\Repository\ParticipantsRepository;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass=ParticipantsRepository::class)
 */


class Participants
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $idUser;

    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $isPresent;

    /**
     * @MongoDB\ReferenceOne(targetDocument=Events::class, inversedBy="participants", storeAs="id")
     */
    public $eventsId;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * @param mixed $idUser
     */
    public function setIdUser($idUser): void
    {
        $this->idUser = $idUser;
    }

    /**
     * @return mixed
     */
    public function getIsPresent()
    {
        return $this->isPresent;
    }

    /**
     * @param mixed $isPresent
     */
    public function setIsPresent($isPresent): void
    {
        $this->isPresent = $isPresent;
    }

    /**
     * @return mixed
     */
    public function getEventsId()
    {
        return $this->eventsId;
    }
    /**
     * @param mixed $eventsId
     */
    public function setGroupId(Groups $groupId): void
    {
        $this->groupId = $groupId;
    }


}