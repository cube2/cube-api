<?php


namespace App\Repository;


use Doctrine\ODM\MongoDB\Repository\DocumentRepository;

class ContentsRepository extends DocumentRepository
{
    public function getAllContentForIdGroup($id) : array
    {
        return $this->findBy(['groupId' => $id],['createdAt'=>'DESC']);
    }

    public function getAllContentForIdEvent($id) : array
    {
        return $this->findBy(['eventId' => $id],['createdAt'=>'DESC']);
    }

}