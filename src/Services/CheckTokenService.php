<?php


namespace App\Services;


use App\Document\User;
use Doctrine\ODM\MongoDB\DocumentManager;

class CheckTokenService
{
    private $dm;

    public function __construct(DocumentManager $dm)
    {
        $this->dm = $dm;
    }

    public function validUser($id, $token) :bool
    {
        $user =  $this->dm->getRepository(User::class)->find($id);
        if ($user){
            if ($user->getToken() === $token){
                return true;
            }
        }
        return false;
    }
}