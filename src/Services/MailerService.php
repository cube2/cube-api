<?php


namespace App\Services;


use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Twig\Environment;

class MailerService
{
    private $mailer;
    private $twig;

    public function __construct(MailerInterface $mailer,Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;

    }

    public function sendEmail(string $to,string $subject,string $template, array $params):void
    {
        $email = (new Email())
            ->from($_ENV['SMTP_EMAIL'])
            ->to($to)
            ->priority(Email::PRIORITY_HIGH)
            ->subject($subject)
            ->html(
                    $this->twig->render($template,$params),'text/html'
            );
        $this->mailer->send($email);
    }

}